﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddleAI : MonoBehaviour {

    private Rigidbody rigidbody;
    public float speed = 20f;
    public float manualSpeed = 300f;

    public float force = 10f;

    public Transform Goal;
    public Rigidbody Puck;

    public Transform startingPos;



    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    //private Vector3 GetMousePosition()
    //{
    //    // create a ray from the camera
    //    // passing through the mouse position
    //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //    // find out where the ray intersects the XZ plane
    //    Plane plane = new Plane(Vector3.up, Vector3.zero);
    //    float distance = 0;
    //    plane.Raycast(ray, out distance);
    //    return ray.GetPoint(distance);
    //}

    // Update is called once per frame
    void Update()
    {

    }

    //void FixedUpdate()
    //{
    //    Vector3 pos = GetMousePosition();
    //    Vector3 dir = pos - rigidbody.position;
    //    Vector3 vel = dir.normalized * speed;
    //    // check is this speed is going to overshoot the target
    //    float move = speed * Time.fixedDeltaTime;
    //    float distToTarget = dir.magnitude;
    //    if (move > distToTarget)
    //    {
    //        // scale the velocity down appropriately
    //        vel = vel * distToTarget / move;
    //    }
    //    rigidbody.velocity = vel;
    //}

    //void FixedUpdate()
    //{
    //    Vector3 pos = GetMousePosition();
    //    Vector3 dir = pos - rigidbody.position;
    //    rigidbody.AddForce(dir.normalized * force);
    //}

    void FixedUpdate()
    {
        rigidbody.velocity = Vector3.zero;
        Vector3 pos = new Vector3(Puck.transform.localPosition.x, 0, Puck.transform.localPosition.z);
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;

        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            vel = vel * distToTarget / move;
        }
        rigidbody.velocity = vel;

        //// get the input values
        //Vector3 heading;
        //Vector3 direction;
        //heading = Goal.position + Puck.position;
        //direction = heading / heading.magnitude;

        //direction.x = Puck.position.x;
        //direction.z = Puck.position.z;
        //direction.y = 0;

        //// scale by the maxSpeed parameter
        //Vector3 velocity = direction * manualSpeed;
        //// move the object
        //rigidbody.velocity = velocity * Time.deltaTime;
    }

    public void ResetPosition()
    {
        // teleport to the starting position
        rigidbody.MovePosition(startingPos.position);
        // stop it from moving
        rigidbody.velocity = Vector3.zero;
    }



    //void OnDrawGizmos()
    //{
    //    // draw the mouse ray
    //    Gizmos.color = Color.yellow;
    //    Vector3 pos = GetMousePosition();
    //    Gizmos.DrawLine(Camera.main.transform.position, pos);
    //}
}
